package com.yh.blink.broadcast;

import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.io.IOException;

public class BroadcastTest {
    public static void main(String[] args) throws IOException {
        StreamExecutionEnvironment env=StreamExecutionEnvironment.getExecutionEnvironment();

        DataStreamSource<Long> longDataStreamSource = env.fromElements(1L, 2L);


        longDataStreamSource.broadcast();


    }
}
