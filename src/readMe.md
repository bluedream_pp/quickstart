## 了解什么是 Metric

看看下面这个 blog , 应该会有一个大概的了解：

http://wuchong.me/blog/2015/08/01/getting-started-with-metrics/

## metric 的类型

- 单值
    - counter：计数器，只能返回一个单个的 long 值
    - gauges: 比 counter 更加灵活，可以将类里面的成员变量的组合起来返回给 report
- 复合值
    - meter: 返回的是一个平均值，例如 tps 值。meter 提供给我们了一个 mark(long) 这样的一个函数，我们可以使用这个函数在接收或者发送数据的时候，调用 mark(record number)
      meter 就会自动计算出总体 tps、前 5 分钟 tps、前 15 分钟 tps。
    - Histograms: 将我们输入的数字集合的 最小值、最大值、中间值、中位数即算出了


